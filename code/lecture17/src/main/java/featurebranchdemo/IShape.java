package featurebranchdemo;

/**
 * Interface to represent all Sahape objects
 * 
 * @author Swetha Rajagopal
 * @version 10/31/2023
 */
public interface IShape {
    double getArea();
}
