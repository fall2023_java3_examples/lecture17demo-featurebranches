package featurebranchdemo;

/**
 * Circle class to represent Circle objects
 * 
 * @author Swetha Rajagopal
 * @version 10/31/2023
 */
public class Circle implements IShape {
    /**
     * Constructs Circle object
     * 
     * @param side represents the radius of Circle
     * @throws IllegalArgumentException if radius is less than or equal to 0
     */
    public Circle(double side){
        throw new UnsupportedOperationException("Not yet implemented");
    }
    public double getSide(){
        throw new UnsupportedOperationException("Not yet implemented");
    }
    public double getArea(){
        throw new UnsupportedOperationException("Not yet implemented");
    }
    
}
